# Source: Geeksforgeeks
# Function to find the maximum contiguous subarray
# from sys import maxint

maxint = 1000000000
import requests

class Kadane:

    def maxSubArraySum(self, a, size):

        if not isinstance(size, int):
            raise TypeError("size should be an integer.")

        max_so_far = -maxint - 1
        max_ending_here = 0

        for i in range(0, size):

            if not isinstance(a[i], int):
                raise TypeError("Array elements should be integers.")

            max_ending_here = max_ending_here + a[i]
            if (max_so_far < max_ending_here):
                max_so_far = max_ending_here

            if max_ending_here < 0:
                max_ending_here = 0
        return max_so_far


    def code_link(self, link):
        response = requests.get(f'{link}')
        if response.ok:
            return 'Code is there.'
        else:
            return 'Bad Response!'



# a = [-13, -3, -25, -20, -3, -16, -23, -12, -5, -22, -15, -4, -7]
# print(maxSubArraySum(a, len(a)))

with open("./resources/input.txt") as f:
    for line in f:
        lis = line.split(" ")

    temp_map = map(int, lis)
    num_list = list(temp_map)
    size = num_list[0]
    arr = num_list[1:]

    print(size)
    print(arr)

obj = Kadane()
res = obj.maxSubArraySum(arr, size)
print(res)